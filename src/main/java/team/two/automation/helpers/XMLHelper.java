package team.two.automation.helpers;

import org.testng.ITestContext;
import team.two.automation.commons.GlobalVariable;

public class XMLHelper {
    /**
     * Read value of environment parameter
     *
     * @param context
     */
    public static void getTestDataType(ITestContext context) {

        String value = context.getCurrentXmlTest().getParameter("environment");

        GlobalVariable.setTestDataType(value);
    }
}

package team.two.automation.helpers;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.testng.ITestContext;
import team.two.automation.commons.GlobalVariable;

public class ExtentReportHelper {
    /**
     * Set up ExtentReports and store its related variables in GlobalVariable class
     *
     * @param context
     */
    private static final ThreadLocal<ExtentTest> EXTENT_TEST_THREAD_LOCAL = new ThreadLocal<>();

    public static ExtentTest getExtentTest() {
        return EXTENT_TEST_THREAD_LOCAL.get();
    }

    public static void setExtentTest(ExtentTest test) {
        EXTENT_TEST_THREAD_LOCAL.set(test);
    }

    public static void setUp(ITestContext context) {
        GlobalVariable.setHtmlReporter(new ExtentHtmlReporter("src/test/test-output/" + context.getName() + ".html"));
        GlobalVariable.setExtentReports(new ExtentReports());
        GlobalVariable.getExtentReports().attachReporter(GlobalVariable.getHtmlReporter());
    }
}

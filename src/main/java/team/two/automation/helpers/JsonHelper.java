package team.two.automation.helpers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import team.two.automation.commons.GlobalVariable;

import java.io.FileReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.List;

public class JsonHelper {
    private static final String BASE_PATH = "src/test/resources/data/";

    /**
     * Read data from json file using Gson with generic type
     *
     * @param fileName
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> getDataFromJson(String fileName, Class<T> clazz) {
        String filePath = BASE_PATH + "/" + GlobalVariable.getTestDataType() + "/" + fileName;
        Gson gson = new Gson();
        try {
            Reader reader = new FileReader(filePath);

            Type typeOfT = TypeToken.getParameterized(List.class, clazz).getType();
            return gson.fromJson(reader, typeOfT);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}


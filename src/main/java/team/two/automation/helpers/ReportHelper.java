package team.two.automation.helpers;

import com.aventstack.extentreports.MediaEntityBuilder;
import team.two.automation.reports.AbstractAssertion;

import java.io.IOException;

public class ReportHelper {

    /**
     * Create logFail for Extent report
     *
     * @param expected
     * @param actual
     * @param path
     */
    public static void logFail(String expected, String actual, String path) {
        try {
            ExtentReportHelper.getExtentTest().fail(AbstractAssertion.formatFailedMessage(expected, actual), MediaEntityBuilder.createScreenCaptureFromPath(path).build());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create logFail for TestListener
     *
     * @param message
     * @param path
     */
    public static void logFail(String message, String path) {
        try {
            ExtentReportHelper.getExtentTest().fail(AbstractAssertion.formatFailedExecuteMessage(message), MediaEntityBuilder.createScreenCaptureFromPath(path).build());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

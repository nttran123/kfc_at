package team.two.automation.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageHelper {
    private static final int ELEMENT_TIME_WAIT = 10;
    private static final WebDriverWait webDriverWait = new WebDriverWait(DriverHelper.getDriver(), ELEMENT_TIME_WAIT);

    /**
     * Wait for options of dynamic selector get loaded
     *
     * @param locator
     */
    public static void waitForOptionAppear(By locator) {
        webDriverWait.until(ExpectedConditions.numberOfElementsToBeMoreThan(locator, 0));
    }

    /**
     * Wait for element appear
     *
     * @param locator
     */
    public static void waitForElementAppear(By locator) {
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static void waitForElementClickable(By locator) {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(locator));
    }

}

package team.two.automation.reports;

public abstract class AbstractAssertion {
    private static final String FAILED_MESSAGE_FORMAT = "<br>&emsp;&emsp;&emsp;&emsp;-Expected Result: %s</br><br>&emsp;&emsp;&emsp;&emsp;-Actual Result: %s</br><br>";
    private static final String FAILED_EXECUTE_MESSAGE_FORMAT ="<br>&emsp;&emsp;&emsp;&emsp;-There was an error: %s</br>";

    /**
     * Custom failed message
     *
     * @param expected
     * @param actual
     * @return
     */
    public static String formatFailedMessage(String expected, String actual) {

        return String.format(FAILED_MESSAGE_FORMAT, expected, actual);
    }

    public static String formatFailedExecuteMessage(String error){
        return String.format(FAILED_EXECUTE_MESSAGE_FORMAT, error);
    }
}

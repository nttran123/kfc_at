package team.two.automation.ui;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

public class CheckBox extends BaseElement {
    public CheckBox(String locator) {
        super(locator);
    }

}

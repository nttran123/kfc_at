package team.two.automation.ui;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import team.two.automation.helpers.DriverHelper;
import team.two.automation.helpers.ElementHelper;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BaseElement {
    private String locator;

    public WebElement getElement() {
        return DriverHelper.getDriver().findElement(ElementHelper.getLocatorFromString(locator));
    }

    public By getElementLocator() {
        return ElementHelper.getLocatorFromString(locator);
    }

    public void click() {
        getElement().click();
    }

    public String getText() {
        return getElement().getText();
    }
}

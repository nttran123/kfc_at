package team.two.automation.ui;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor

public class RadioButton extends BaseElement {

    public RadioButton(String locator) {
        super(locator);
    }

}

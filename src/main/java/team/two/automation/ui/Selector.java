package team.two.automation.ui;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openqa.selenium.support.ui.Select;

@Getter
@Setter
@NoArgsConstructor
public class Selector extends BaseElement {

    public Selector(String locator) {
        super(locator);
    }

    public void selectByValue(String value) {
        new Select(getElement()).selectByValue(value);
    }

    public void selectByVisibleText(String text) {
        new Select(getElement()).selectByVisibleText(text);
    }
}

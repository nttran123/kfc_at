package team.two.automation.listener;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import team.two.automation.commons.GlobalVariable;
import team.two.automation.helpers.DriverHelper;
import team.two.automation.helpers.ExtentReportHelper;
import team.two.automation.helpers.ReportHelper;
import team.two.automation.helpers.XMLHelper;
import team.two.automation.reports.HtmlLog;
import team.two.automation.reports.TestUtilities;

import java.io.IOException;

public class BaseTestListener implements ITestListener {

    @Override
    public void onTestStart(ITestResult result) {
        System.out.println(result.getName() + "-----on test start");
        DriverHelper.setUp();
        ExtentReportHelper.setExtentTest(GlobalVariable.getExtentReports().createTest(result.getName()));
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        HtmlLog.stepInfo("Test case finished.");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        String path = null;
        try {
            path = TestUtilities.takeScreenShot(result.getTestName());
        } catch (IOException e) {
            e.printStackTrace();
        }
        ReportHelper.logFail(result.getMethod().getMethodName(), path);
        HtmlLog.stepInfo("Test case finished.");
    }

    @Override
    public void onTestSkipped(ITestResult result) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {
        System.out.println(context.getName() + "---- Suite start");
        ExtentReportHelper.setUp(context);
        XMLHelper.getTestDataType(context);
    }

    @Override
    public void onFinish(ITestContext context) {
        System.out.println(context.getName() + "-----Suite end.");
    }
}

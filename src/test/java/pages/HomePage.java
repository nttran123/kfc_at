package pages;

import org.openqa.selenium.By;
import team.two.automation.helpers.DriverHelper;
import team.two.automation.helpers.PageHelper;
import team.two.automation.reports.SoftAssertion;
import team.two.automation.ui.Button;
import team.two.automation.ui.Text;

public class HomePage extends BasePage{
    private final Button selectLocationButton = new Button("xpath = //div[@class= 'filter-option-inner-inner']");
    private final String locationOption = "xpath = //ul[@class ='dropdown-menu inner show']/li/a/span[text()='%s']";
    private final String itemsLocation = "xpath = //div[@class = 'card ']";
    private final Text languageText = new Text("xpath = //div[contains(@class,'select_lang ')]/div/a");
    private final Button startOrderButton = new Button("xpath = //a/span[text()='Start my order']");
    private final Button changeLanguageButton = new Button("xpath = //div[contains(@class,'select_lang')]/div");
    private final Button engButton = new Button("xpath = //div[@class='dropdown-menu show']/a[2]");
    private final String orderButtonLocator = "xpath = //div[@class='card-body']/h5[@class='card-title' and span[text()='%s']]/following-sibling::div[@class='row_btn']/div/div[@class = 'col order-lg-2']";
    private final Text alaCarte = new Text("xpath = //li[@id = 'headerMenuALaCarte']/a/span");
    private Button userLocation;
    private Button orderButton;

    public void selectLocation(String location) {
        PageHelper.waitForElementAppear(selectLocationButton.getElementLocator());
        selectLocationButton.click();
        userLocation = new Button(String.format(locationOption, location));
        PageHelper.waitForElementAppear(userLocation.getElementLocator());
        userLocation.click();
        startOrderButton.click();
    }

    public void changeLanguageToEng() {
        PageHelper.waitForElementAppear(changeLanguageButton.getElementLocator());
        changeLanguageButton.click();
        PageHelper.waitForElementAppear(engButton.getElementLocator());
        engButton.click();
    }

    public void checkWebChangedToEng() {
        PageHelper.waitForElementAppear(alaCarte.getElementLocator());

        String text = alaCarte.getText();

        SoftAssertion.assertEquals("A La Carte", text, "Verify that website changed to english.");
    }

    public void orderProductByTitle(String title) {
        orderButton = new Button(String.format(orderButtonLocator, title));
        PageHelper.waitForElementClickable(orderButton.getElementLocator());
        orderButton.click();
    }
     public void checkLangToEng(){
        PageHelper.waitForOptionAppear(languageText.getElementLocator());
         String actual = languageText.getText();
         SoftAssertion.assertEquals("EN",actual,"Verify the page is displayed in English");
     }
     public void checkItemsDisplayed(){
        PageHelper.waitForElementAppear(By.xpath("//div[@class = 'card ']"));
         Boolean checkDisplayed = DriverHelper.getDriver().findElement(By.xpath("//div[@class = 'card ']")).isDisplayed();
         SoftAssertion.assertTrue(checkDisplayed, "Verify all items are displayed");
     }

}

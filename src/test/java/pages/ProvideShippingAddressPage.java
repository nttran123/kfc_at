package pages;

import model.User;
import team.two.automation.helpers.PageHelper;
import team.two.automation.reports.SoftAssertion;
import team.two.automation.ui.Button;
import team.two.automation.ui.Text;
import team.two.automation.ui.TextBox;

public class ProvideShippingAddressPage {
    private final TextBox fullName = new TextBox("id = inputFullname");
    private final TextBox phone = new TextBox("id = inputPhone");
    private final TextBox email = new TextBox("id = inputEmail");
    private final Button city = new Button("xpath = //button[@data-id = 'select_city']");
    private final Button district = new Button("xpath = //button[@data-id = 'select_district']");
    private final Button ward = new Button("xpath = //button[@data-id = 'select_ward']");
    private final String location = "xpath = //ul[@class='dropdown-menu inner show']/li/a/span[text()='%s']";
    private final TextBox address = new TextBox("id = inputAddress");
    private final Text estimateDeliveryTime = new Text("id = estimateDeliveryTime");
    private final Button continueButton = new Button("xpath = //button[contains(@class,'btn_web btn_red')]/span[text() = 'Continue']");
    private final Button seeDetailButton = new Button("xpath = //div[@id = 'deliveryWard']/a/b");
    private final Button loginButton = new Button("xpath = //a[contains(@class,'mt-0 kfc_call_login')]");
    private final Text deliveryDistrict = new Text("xpath = //div[@id= 'popupDeliveryDetailContent']/p[2]/b");
    private final Button deliveryDetailCloseButton = new Button("xpath = //div[@id= 'popup_deliveryDetail']/button");
    private final Text pageTitle = new Text("xpath = //div[@class = 'new_head_top']/h2");
    private final Button selectRestaurant = new Button("xpath = //div[@class='form_book_item']/div[1]");
    private Button cityOption;
    private Button districtOption;
    private Button wardOption;

    public void provideAddress(User user) {
        cityOption = new Button(String.format(location, user.getCity()));
        districtOption = new Button(String.format(location, user.getDistrict()));
        wardOption = new Button(String.format(location, user.getWard()));
        fullName.enterText(user.getFullName());
        phone.enterText(user.getPhone());
        email.enterText(user.getEmail());
        city.click();
        PageHelper.waitForElementAppear(cityOption.getElementLocator());
        cityOption.click();
        district.click();
        PageHelper.waitForElementAppear(districtOption.getElementLocator());
        districtOption.click();
        ward.click();
        PageHelper.waitForElementAppear(wardOption.getElementLocator());
        wardOption.click();
        address.enterText(user.getAddress());
        PageHelper.waitForElementAppear(selectRestaurant.getElementLocator());
        selectRestaurant.click();
        continueButton.click();
    }

    public void checkDistrictCorrect(String district) {
        PageHelper.waitForElementAppear(deliveryDistrict.getElementLocator());

        String text = deliveryDistrict.getText();

        SoftAssertion.assertEquals(district, text, "Verify that delivery district is equals to provided district: " + district);
    }

    public void checkPageCorrect() {
        PageHelper.waitForElementAppear(pageTitle.getElementLocator());

        String text = pageTitle.getText();

        SoftAssertion.assertEquals("PLEASE PROVIDE YOUR SHIPPING ADDRESS!", text, "Verify that page title is: PLEASE PROVIDE YOUR SHIPPING ADDRESS!");
    }

}

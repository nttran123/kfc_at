package pages;

import team.two.automation.helpers.PageHelper;
import team.two.automation.reports.SoftAssertion;
import team.two.automation.ui.Text;


public class ConfirmSignUpPage {

    private final Text informText = new Text("xpath = //div[@class='membership_text text_head_membership text-center']/p[1]");
    private final Text confirmEmail = new Text("xpath = //div[@class='membership_text text_head_membership text-center']/p[2]/strong");


    public void checkIfConfirmSignUpPageAppear() {
        PageHelper.waitForElementAppear(informText.getElementLocator());

        String actual = "Thank you for signing up!";

        String text = informText.getText();

        SoftAssertion.assertEquals(actual, text, "Verify that the browser navigate to confirm sign up page");
    }

    public void checkEmailCorrect(String email) {
        PageHelper.waitForElementAppear(confirmEmail.getElementLocator());

        String text = confirmEmail.getText();

        SoftAssertion.assertEquals(email, text, "Verify confirm email is equal to " + email);
    }

}


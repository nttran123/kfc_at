package pages;

import model.User;
import team.two.automation.helpers.DriverHelper;
import team.two.automation.helpers.PageHelper;
import team.two.automation.reports.SoftAssertion;
import team.two.automation.ui.Button;
import team.two.automation.ui.Text;
import team.two.automation.ui.TextBox;

    public class BasePage {
        protected final Button cartButton = new Button("id = cartBox");
        protected final Text popupSignInTitle = new Text("xpath = //div[@id='popup_Signin']/h5/span");
        protected final Button signUpButton = new Button("xpath = //a[@href = 'https://kfcvietnam.com.vn/en/sign-up.html']/u");
        protected final Button popupSignInButton = new Button("xpath = //a[contains(@class,'kfc_call_login') and @data-toggle='dropdown']");
        protected final Button signInButton = new Button("xpath = //button[@type='submit' and @class ='btn btn_nhut btn-danger btn-block']");
        protected final Button userName = new Button("xpath = //a[@class = 'pc btn btn-default dropdown-toggle active']");
        protected final TextBox inputLoginEmail = new TextBox("xpath = //input[@id='inputLoginEmail']");
        protected final TextBox inputLoginPassword = new TextBox("id = inputLoginPassword");
        protected final Text recipientName = new Text("xpath = //div[@class = 'text-uppercase'][1]/strong");
        protected final Button selectLocationButton = new Button("xpath = //div[@class= 'filter-option-inner-inner']");
        protected final String locationOption = "xpath = //ul[@class ='dropdown-menu inner show']/li/a/span[text()='%s']";
        protected final Button startOrderButton = new Button("xpath = //a/span[text()='Start my order']");
        protected Button userLocation;


        public void selectLocation(String location) {
            PageHelper.waitForElementClickable(selectLocationButton.getElementLocator());
            selectLocationButton.click();
            userLocation = new Button(String.format(locationOption, location));
            PageHelper.waitForElementAppear(userLocation.getElementLocator());
            userLocation.click();
            startOrderButton.click();
        }

        public void checkRecipientInfoCorrect(User user) {
            PageHelper.waitForElementAppear(recipientName.getElementLocator());
            SoftAssertion.assertEquals(user.getFullName(), recipientName.getText(), "Verify recipient name is equals to " + user.getFullName());
        }

        public void signIn(User user) {
            PageHelper.waitForElementAppear(popupSignInButton.getElementLocator());
            popupSignInButton.click();
            checkPopupSignInDisplayed();
            PageHelper.waitForElementClickable(inputLoginEmail.getElementLocator());
            inputLoginEmail.enterText(user.getEmail());
            inputLoginPassword.enterText(user.getPassword());
            signInButton.click();
        }

        public void navigateToCartPage() {
            PageHelper.waitForElementAppear(cartButton.getElementLocator());
            cartButton.click();
        }

        public void navigateToRegisterPage() {
            PageHelper.waitForElementAppear(popupSignInButton.getElementLocator());
            popupSignInButton.click();
            PageHelper.waitForElementAppear(signUpButton.getElementLocator());
            signUpButton.click();
        }

        public void checkUserLoggedIn(String name) {
            PageHelper.waitForElementAppear(userName.getElementLocator());
            SoftAssertion.assertEquals(name, userName.getText(), "Verify that user name is displayed.");
        }

        public void checkPopupSignInDisplayed(){
            PageHelper.waitForElementAppear(popupSignInTitle.getElementLocator());
            String actual = popupSignInTitle.getText();
            SoftAssertion.assertEquals("Sign in", actual, "Verify that popup Sign in is displayed");
        }
        public void checkPageCorrect(String expected) {

            String text = DriverHelper.getDriver().getTitle();

            SoftAssertion.assertEquals(expected, text, "Verify that page title is: " + expected );
        }

    }


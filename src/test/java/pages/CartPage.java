package pages;

import team.two.automation.helpers.PageHelper;
import team.two.automation.reports.SoftAssertion;
import team.two.automation.ui.Button;
import team.two.automation.ui.Text;

public class CartPage extends BasePage {
    private final Text orderedItemName = new Text("xpath = //div[@class ='cart_new_item']/h3[@class='title_name_sp']");
    private final Text cartEmptyText = new Text("xpath = //p[@class = 'txt_empty']/span");
    private final Button processToPaymentButton = new Button("xpath = //div[contains(@class,'sidebar_btn')]/a/span[text()='Proceed to payment']");
    private final Button closeInformPopUpButton = new Button("xpath = //div[@id= 'popup_Thongbao']/button[@title='Close']");
    private final String deleteOrderItemLocator = "xpath = //h3[text()='%s']//parent::div/div//a[@class='cart_close']";
    private final String itemQuantityIncrementButtonLocator = "xpath = //h3[text()='%s']//parent::div/div[contains(@class,'total_price_new')]/div/a[@class='plus']";
    private final String itemQuantityLocator = "xpath = //h3[text()='%s']//parent::div/div[contains(@class,'total_price_new')]/div/span[contains(@id,'qt')]";
    private Text itemQuantity;
    private Button itemQuantityIncrementButton;
    private Button deleteOrderedItem;

    public void checkCartItemMatchedOrderedItem(String name) {
        SoftAssertion.assertEquals(name, orderedItemName.getText(), "Verify cart item name is equals to " + name);
    }

    public void processToBuy() {
        PageHelper.waitForElementAppear(processToPaymentButton.getElementLocator());
        processToPaymentButton.click();
        PageHelper.waitForElementAppear(closeInformPopUpButton.getElementLocator());
        closeInformPopUpButton.click();
    }

    public void checkIncreasedQuantity(String itemName) {
        itemQuantity = new Text(String.format(itemQuantityLocator, itemName));
        PageHelper.waitForElementAppear(itemQuantity.getElementLocator());
        String beforeQuantityText = itemQuantity.getText();
        int expectedQuantity = Integer.parseInt(beforeQuantityText) + 1;
        itemQuantityIncrementButton = new Button(String.format(itemQuantityIncrementButtonLocator, itemName));
        itemQuantityIncrementButton.click();
        PageHelper.waitForElementAppear(itemQuantity.getElementLocator());
        String afterQuantityText = itemQuantity.getText();
        int actualQuantity = Integer.parseInt(afterQuantityText);
        SoftAssertion.assertEquals(expectedQuantity, actualQuantity, "Verify that item quantity is increased by 1");
    }

    public void checkCartEmpty() {
        String expected = "Your Cart is empty?";
        PageHelper.waitForElementAppear(cartEmptyText.getElementLocator());
        String actual = cartEmptyText.getText();
        SoftAssertion.assertEquals(expected, actual, "Verify the cart is empty.");
    }

    public void deleteItemByName(String name) {
        deleteOrderedItem = new Button(String.format(deleteOrderItemLocator, name));
        PageHelper.waitForElementAppear(deleteOrderedItem.getElementLocator());
        deleteOrderedItem.click();
    }

}

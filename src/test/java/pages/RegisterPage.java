package pages;

import model.User;
import team.two.automation.helpers.PageHelper;
import team.two.automation.ui.Button;
import team.two.automation.ui.CheckBox;
import team.two.automation.ui.Selector;
import team.two.automation.ui.TextBox;

public class RegisterPage extends BasePage{

    private final TextBox fullName = new TextBox("id = inputFullname");
    private final TextBox email = new TextBox("id = inputEmail");
    private final TextBox password = new TextBox("id = inputPassword");
    private final TextBox rePassword = new TextBox("id = inputRePassword");
    private final TextBox phone = new TextBox("id = inputPhone");
    private final CheckBox sex = new CheckBox("xpath = //label[@class='custom-control-label'][text() = 'Male']");
    private final Selector dayOfBirth = new Selector("name = birthday_day");
    private final Selector monthOfBirth = new Selector("name = birthday_month");
    private final Selector yearOfBirth = new Selector("name = birthday_year");
    private final Selector city = new Selector("id = selectCity");
    private final Selector district = new Selector("id = selectDistrict");
    private final Selector ward = new Selector("id = selectWard");
    private final Button registerButton = new Button("xpath = //button[@class='btn btn_nhut btn-danger btn-block']/span[text()='Register']");

    public void registerKFC(User user) {

        fullName.enterText(user.getFullName());
        email.enterText(user.getEmail());
        password.enterText(user.getPassword());
        rePassword.enterText(user.getRePassword());
        phone.enterText(user.getPhone());
        sex.click();
        dayOfBirth.selectByValue(user.getDayOfBirth());
        monthOfBirth.selectByValue(user.getMonthOfBirth());
        yearOfBirth.selectByValue(user.getYearOfBirth());
        city.selectByVisibleText(user.getCity());
        PageHelper.waitForOptionAppear(district.getElementLocator());
        district.selectByVisibleText(user.getDistrict());
        PageHelper.waitForOptionAppear(ward.getElementLocator());
        ward.selectByVisibleText(user.getWard());
        registerButton.click();
    }
}

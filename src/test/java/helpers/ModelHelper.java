package helpers;

import model.Product;
import model.User;
import team.two.automation.helpers.JsonHelper;

import java.util.List;

public class ModelHelper {
    /**
     * Find and return a user by email
     *
     * @param email
     * @return
     */
    public static User getTestUserByEmail(String email) {
        List<User> users = JsonHelper.getDataFromJson("user.json", User.class);
        User testUser = users.stream().filter(user -> email.equals(user.getEmail())).findAny().orElse(null);
        return testUser;
    }

    public static Product getTestProductByTitle(String title) {
        List<Product> products = JsonHelper.getDataFromJson("product.json", Product.class);
        Product testProduct = products.stream().filter(product -> title.equals(product.getTitle())).findAny().orElse(null);
        return testProduct;
    }
}

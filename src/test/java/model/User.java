package model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String fullName;
    private String email;
    private String password;
    private String rePassword;
    private String phone;
    private String dayOfBirth;
    private String monthOfBirth;
    private String yearOfBirth;
    private String city;
    private String district;
    private String ward;
    private String address;
}

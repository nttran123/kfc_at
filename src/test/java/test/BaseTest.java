package test;

import org.testng.annotations.*;
import team.two.automation.commons.GlobalVariable;
import team.two.automation.helpers.DriverHelper;
import team.two.automation.reports.HtmlLog;

public class BaseTest {
    private final String BASE_URL = "https://kfcvietnam.com.vn/en/bat-dau-dat-hang.html";

    @AfterMethod
    protected void resetStepIndex() {
        HtmlLog.resetStepIdx();
    }

    /**
     * navigate to a page by its URL
     */
    protected void navigateByURL() {
        DriverHelper.getDriver().get(BASE_URL);
    }

    @AfterClass
    protected void closeBrowser() {
        DriverHelper.closeBrowser();
        GlobalVariable.getExtentReports().flush();
    }

}

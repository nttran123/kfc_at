package test;

import helpers.ModelHelper;
import model.User;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.HomePage;
import team.two.automation.reports.HtmlLog;

public class TestLogin extends BaseTest {
    private final String TEST_EMAIL = "ngoc@123.com";
    User user = null;
    HomePage homePage = new HomePage();

    @BeforeClass
    protected void getTestData() {
        user = ModelHelper.getTestUserByEmail(TEST_EMAIL);
    }

    @Test
    public void testLogInUser() {
        navigateByURL();

        HtmlLog.stepInfo("Select order location");
        homePage.selectLocation(user.getCity());

        HtmlLog.stepInfo("Change language to English");
        homePage.changeLanguageToEng();

        HtmlLog.stepInfo("Check page changed to English");
        homePage.checkLangToEng();

        HtmlLog.stepInfo("Sign in with email " + user.getEmail());
        homePage.signIn(user);

        HtmlLog.stepInfo("Verify user logged in");
        homePage.checkUserLoggedIn(user.getFullName());

    }
}

package test;

import helpers.ModelHelper;
import model.Product;
import model.User;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.CartPage;
import pages.ConfirmSignUpPage;
import pages.HomePage;
import pages.RegisterPage;
import team.two.automation.reports.HtmlLog;


public class TestRegisterKfc extends BaseTest {
    private final String TEST_PRODUCT_TITLE = "COMBO FRIED CHICKEN A";
    private final String TEST_EMAIL = "ngoc@123.com";
    User user = null;
    Product product = null;
    RegisterPage kfcRegister = new RegisterPage();
    ConfirmSignUpPage confirmSignUp = new ConfirmSignUpPage();
    HomePage homePage = new HomePage();
    CartPage cartPage = new CartPage();

    @BeforeClass
    protected void getTestData() {
        user = ModelHelper.getTestUserByEmail(TEST_EMAIL);
        product = ModelHelper.getTestProductByTitle(TEST_PRODUCT_TITLE);
    }

    @Test
    public void testRegisterUser() {
        /**
         * register with user information
         */
        HtmlLog.stepInfo("Navigate to home page");
        navigateByURL();

        HtmlLog.stepInfo("Select order location");
        homePage.selectLocation(user.getCity());

        HtmlLog.stepInfo("Change language to English");
        homePage.changeLanguageToEng();

        HtmlLog.stepInfo("Check page changed to English");
        homePage.checkLangToEng();

        HtmlLog.stepInfo("Navigate to register page");
        kfcRegister.navigateToRegisterPage();

        HtmlLog.stepInfo("Verify that the website navigated to register page");
        kfcRegister.checkPageCorrect("KFC's membership | KFC Vietnam");

        HtmlLog.stepInfo("Check if all information inputted correctly");
        kfcRegister.registerKFC(user);
        /**
         * navigate to confirm sign up page
         */
        HtmlLog.stepInfo("The browser navigated to confirm signup page");
        confirmSignUp.checkIfConfirmSignUpPageAppear();
        /**
         * check if confirmed email equal to user email
         */
        HtmlLog.stepInfo("Check if confirmed email equal to " + user.getEmail());
        confirmSignUp.checkEmailCorrect("abc");

    }
}

package test;

import helpers.ModelHelper;
import model.Product;
import model.User;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.CartPage;
import pages.HomePage;
import pages.ProvideAddressPage;
import team.two.automation.reports.HtmlLog;

public class TestDeleteItemInCart extends BaseTest {
    private final String TEST_PRODUCT_TITLE = "COMBO FRIED CHICKEN A";
    private final String TEST_EMAIL = "ngoc@123.com";
    Product product = null;
    User user = null;
    HomePage homePage = new HomePage();
    ProvideAddressPage addressPage = new ProvideAddressPage();
    CartPage cartPage = new CartPage();

    @BeforeClass
    protected void getTestData() {
        product = ModelHelper.getTestProductByTitle(TEST_PRODUCT_TITLE);
        user = ModelHelper.getTestUserByEmail(TEST_EMAIL);
    }

    @Test
    public void testDeleteItemInCart() {

        HtmlLog.stepInfo("Navigate to home page");
        navigateByURL();

        HtmlLog.stepInfo("Select order location");
        homePage.selectLocation(user.getCity());

        HtmlLog.stepInfo("Change language to English");
        homePage.changeLanguageToEng();

        HtmlLog.stepInfo("Check page changed to English");
        homePage.checkLangToEng();

        HtmlLog.stepInfo("Check all items are displayed");
        homePage.checkItemsDisplayed();

        HtmlLog.stepInfo("Click order button of " + product.getTitle());
        homePage.orderProductByTitle(product.getTitle());

        HtmlLog.stepInfo("Verify that navigated to provide shipping address page");
        addressPage.checkPageCorrect("Choose Shipping Address");

        HtmlLog.stepInfo("Fill in delivery form");
        addressPage.provideAddress(user);

        HtmlLog.stepInfo("Click place order button on "+ product.getTitle() +" item card.");
        homePage.orderProductByTitle(product.getTitle());

        HtmlLog.stepInfo("Navigate to cart page");
        homePage.navigateToCartPage();

        HtmlLog.stepInfo("Validate item in card matched item selected");
        cartPage.checkCartItemMatchedOrderedItem(product.getTitle());

        HtmlLog.stepInfo("Proceed to delete item");
        cartPage.deleteItemByName(product.getTitle());

        HtmlLog.stepInfo("Verify cart is empty");
        cartPage.checkCartEmpty();

    }
}
